package com.androidegitim.library;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_linearlayout_root)
    LinearLayout rootLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.main_button_show_toast_message)
    public void showToastMessage() {

//        Toast.makeText(this, "Toast Mesaj oluşturuldu.", Toast.LENGTH_SHORT).show();

        Toast.makeText(this, R.string.toast_message_content, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.main_button_show_dialog)
    public void showDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Dialog Başlık");

        builder.setMessage("Dialog mesaj.");

        builder.setNegativeButton("Hayır", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                Toast.makeText(MainActivity.this, "Hayır' a basıldı", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setPositiveButton("Evet", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                Toast.makeText(MainActivity.this, "Evet' a basıldı", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNeutralButton("İptal", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(MainActivity.this, "İptal' a basıldı", Toast.LENGTH_SHORT).show();
            }
        });

        builder.show();

    }

    @OnClick(R.id.main_button_show_library_dialog)
    public void showLibraryDialog() {

//        DialogHelpers.showDialog(this, getString(R.string.dialog_title), getString(R.string.dialog_message),
//                                 getString(R.string.dialog_positive_button), new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                }, getString(R.string.dialog_negative_button), new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                }, getString(R.string.dialog_neutral_button), new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });


        DialogHelpers.showDialog(this, getString(R.string.dialog_title), getString(R.string.dialog_message),
                                 getString(R.string.dialog_positive_button), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, null, null, null, null);
    }

    @OnClick(R.id.main_button_show_snackbar)
    public void showSnackbar() {

        Snackbar.make(rootLinearLayout, "Deneme Metin", Snackbar.LENGTH_LONG).setAction("Tuş", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("LOG", "snackbar tuşuna basıldı");

            }
        }).show();

//        Snackbar.make(rootLinearLayout, "Deneme Metin", Snackbar.LENGTH_LONG).show();
    }
}
